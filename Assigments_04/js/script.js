$(function () {
	$("#nav").load("nav.html");
	$("#video").load("video-section.html");
	$("#product").load("product-section.html");
	$("#people").load("people.html");
	$("#footer").load("footer.html");
	$("#contact").load("contact-section.html");
})