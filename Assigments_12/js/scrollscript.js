$(window).scroll(function () {
	$('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
});

$(window).scroll(function () {
	if ($(this).scrollTop() > 50) {
		$('.logo-img2').css('display', 'block');
		$('.logo-img1').css('display', 'none');
	} else {
		$('.logo-img2').css('display', 'none');
		$('.logo-img1').css('display', 'block');
	}
});

window.onscroll = function () { scrollFunction() };

function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		document.getElementById("myBtn").style.display = "block";
	} else {
		document.getElementById("myBtn").style.display = "none";
	}
}

function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}